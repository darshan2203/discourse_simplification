package de.fim.uni_passau.information_extraction.discourse_simplification.processing;

import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element.DCore;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.sentences.SentencesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 09.12.16.
 */
public class ExtendedProcessor extends Processor {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public List<DCore> processWikipedia(int nrArticles, Integer minArticleSentences, ProcessingType type, boolean shuffleSentences, Integer maxSentenceLength, Integer maxSentences) {
        return processWikipedia(nrArticles, minArticleSentences, new ProcessMethod() {
            @Override
            public List<DCore> process(List<String> sentences) {
                return ExtendedProcessor.this.process(sentences, type, shuffleSentences, maxSentenceLength, maxSentences);
            }
        });
    }

    public List<DCore> processWikipediaArticles(List<String> articles, ProcessingType type, boolean shuffleSentences, Integer maxSentenceLength, Integer maxSentences) {
        return processWikipediaArticles(articles, new ProcessMethod() {
            @Override
            public List<DCore> process(List<String> sentences) {
                return ExtendedProcessor.this.process(sentences, type, shuffleSentences, maxSentenceLength, maxSentences);
            }
        });
    }

    public List<DCore> process(File file, ProcessingType type, boolean shuffleSentences, Integer maxSentenceLength, Integer maxSentences) throws FileNotFoundException {
        List<String> sentences = SentencesUtils.splitIntoSentencesFromFile(file);
        return process(sentences, type, shuffleSentences, maxSentenceLength, maxSentences);
    }

    public List<DCore> process(String text, ProcessingType type, boolean shuffleSentences, Integer maxSentenceLength, Integer maxSentences) {
        List<String> sentences = SentencesUtils.splitIntoSentences(text);
        return process(sentences, type, shuffleSentences, maxSentenceLength, maxSentences);
    }

    public List<DCore> process(List<String> sentences, ProcessingType type, boolean shuffleSentences, Integer maxSentenceLength, Integer maxSentences) {
        return process(filterSentences(sentences, shuffleSentences, maxSentenceLength, maxSentences), type);
    }

    public static List<String> filterSentences(List<String> sentences, boolean shuffleSentences, Integer maxSentenceLength, Integer maxSentences) {

        // select sentences to process
        List<String> res = new ArrayList<>();
        res.addAll(sentences);

        // shuffle
        if (shuffleSentences) {
            Collections.shuffle(res);
        }

        // remove too long sentences
        if (maxSentenceLength != null) {
            res = res.stream().filter(s -> s.length() <= maxSentenceLength).collect(Collectors.toList());
        }

        // limit number of sentences
        if (maxSentences != null) {
            if (res.size() > maxSentences) {
                res = res.subList(0, maxSentences);
            }
        }

        return res;
    }
}
