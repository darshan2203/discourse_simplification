package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.DiscourseTree;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Subordination;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.words.WordsUtils;
import edu.stanford.nlp.ling.Word;

import java.util.List;
import java.util.Optional;

/**
 * Created by Matthias on 05.01.17.
 */
public class RefSubordinationExtraction extends Extraction {
    private final String extractionRule;
    private final Relation relation;
    private final String signalPhrase; // optional
    private final String rightConstituent;
    private final boolean superordinationIsLeft;
    private final Leaf.Type rightConstituentType;

    // binary
    public RefSubordinationExtraction(String extractionRule, Relation relation, List<Word> signalPhraseWords, List<Word> rightConstituentWords, boolean superordinationIsLeft, Leaf.Type rightConstituentType) {
        this.extractionRule = extractionRule;
        this.relation = relation;
        this.signalPhrase = (signalPhraseWords != null)? WordsUtils.wordsToString(signalPhraseWords) : null;
        this.rightConstituent = WordsUtils.wordsToProperSentenceString(rightConstituentWords);
        this.superordinationIsLeft = superordinationIsLeft;
        this.rightConstituentType = rightConstituentType;
    }

    public Optional<DiscourseTree> convert(Leaf currChild) {

        // find previous node to use as a reference
        Optional<DiscourseTree> prevNode = currChild.getPreviousNode();
        if ((prevNode.isPresent()) && (prevNode.get().usableAsReference())) {

            // use prev node as a reference
            prevNode.get().useAsReference();

            Subordination res = new Subordination(
                    extractionRule,
                    relation,
                    signalPhrase,
                    new Leaf(Leaf.Type.DEFAULT, extractionRule, "tmp"),
                    new Leaf(rightConstituentType, extractionRule, rightConstituent),
                    superordinationIsLeft
            );
            res.replaceLeftConstituent(prevNode.get()); // set prev node as a reference

            return Optional.of(res);
        }

        return Optional.empty();
    }
}
