package de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.tner.TNERString;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.tner.TNERToken;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeExtractionUtils;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.words.WordsUtils;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.trees.Tree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthias on 14.12.16.
 */
public class NERStringParser {

    private static final AbstractSequenceClassifier NER_CLASSIFIER = CRFClassifier.getClassifierNoExceptions("edu/stanford/nlp/models/ner/english.all.3class.distsim.crf.ser.gz");

    public static NERString parse(String text) {
        List<NERToken> tokens = new ArrayList<NERToken>();

        String nerString = NER_CLASSIFIER.classifyToString(text);
        String[] nerTokens = nerString.split(" ");

        int idx = 0;
        for (String nerToken : nerTokens) {
            int sep_idx = nerToken.lastIndexOf("/");

            // create text
            String txt = nerToken.substring(0, sep_idx);
            String category = nerToken.substring(sep_idx + 1);
            NERToken token = new NERToken(idx, txt, category);
            tokens.add(token);

            ++idx;
        }

        return new NERString(tokens);
    }

    public static TNERString parse(Tree parseTree) throws NERStringParseException {
        List<TNERToken> tokens = new ArrayList<TNERToken>();

        List<Integer> parseTreeLeafNumbers = ParseTreeExtractionUtils.getLeafNumbers(parseTree, parseTree);
        String nerString = NER_CLASSIFIER.classifyToString(WordsUtils.wordsToString(parseTree.yieldWords()));
        String[] nerTokens = nerString.split(" ");

        if (parseTreeLeafNumbers.size() != nerTokens.length) {
            throw new NERStringParseException("Could not map NER string to parseTree");
        }

        int idx = 0;
        for (String nerToken : nerTokens) {
            int sep_idx = nerToken.lastIndexOf("/");

            // create token
            String text = nerToken.substring(0, sep_idx);
            String category = nerToken.substring(sep_idx + 1);
            TNERToken token = new TNERToken(idx, text, category, parseTree.getNodeNumber(parseTreeLeafNumbers.get(idx)));
            tokens.add(token);

            ++idx;
        }

        return new TNERString(tokens, parseTree);
    }
}
