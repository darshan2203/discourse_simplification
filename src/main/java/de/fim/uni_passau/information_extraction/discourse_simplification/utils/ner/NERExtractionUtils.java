package de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.IndexRange;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthias on 13.12.16.
 */
public class NERExtractionUtils {

    public static List<IndexRange> getNERIndexRanges(NERString nerString) {
        List<IndexRange> res = new ArrayList<IndexRange>();

        for (NERTokenGroup group : nerString.getGroups()) {
            res.add(new IndexRange(group.getFromTokenIndex(), group.getToTokenIndex()));
        }

        return res;
    }
}
