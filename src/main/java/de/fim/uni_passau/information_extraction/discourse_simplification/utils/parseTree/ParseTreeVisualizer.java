package de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;
import edu.stanford.nlp.trees.Tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 13.12.16.
 */
public class ParseTreeVisualizer {

    private static class MyNode implements PrettyTreePrinter.Node {
        private List<PrettyTreePrinter.Node> children;
        private String caption;
        private int nr;

        public MyNode(Tree parseNode, Tree anchor) {
            this.caption = parseNode.value();
            this.children = new ArrayList<PrettyTreePrinter.Node>();
            for (Tree childNode : parseNode.getChildrenAsList()) {
                this.children.add(new MyNode(childNode, anchor));
            }
            this.nr = parseNode.nodeNumber(anchor);
        }

        @Override
        public List<String> getPTPCaption() {
            return Arrays.asList(caption, "#" + nr);
        }

        @Override
        public List<PrettyTreePrinter.Edge> getPTPEdges() {
            return children.stream().map(c -> new PrettyTreePrinter.DefaultEdge("", c, true)).collect(Collectors.toList());
        }

    }

    public static String prettyPrint(Tree parseTree) {
        MyNode node = new MyNode(parseTree, parseTree);
        return PrettyTreePrinter.prettyPrint(node, false);
    }
}
