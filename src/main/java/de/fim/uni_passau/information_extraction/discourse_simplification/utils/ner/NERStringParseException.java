package de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner;

/**
 * Created by Matthias on 13.12.16.
 */
public class NERStringParseException extends Exception {

    public NERStringParseException(String msg) {
        super(msg);
    }
}
