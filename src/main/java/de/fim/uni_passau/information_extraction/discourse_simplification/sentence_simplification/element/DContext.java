package de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 12.01.17.
 */
public class DContext implements PrettyTreePrinter.Node {
    private final String text;
    private final int sentenceIdx;
    private final String notSimplifiedText;
    private List<SContext> sContexts;

    public DContext(String text, int sentenceIdx, String notSimplifiedText) {
        this.text = text;
        this.sentenceIdx = sentenceIdx;
        this.notSimplifiedText = notSimplifiedText;
        this.sContexts = new ArrayList<SContext>();
    }

    public void addSContext(SContext sContext) {
        this.sContexts.add(sContext);
    }

    public String getText() {
        return text;
    }

    public int getSentenceIdx() {
        return sentenceIdx;
    }

    public String getNotSimplifiedText() {
        return notSimplifiedText;
    }

    public List<SContext> getSContexts() {
        return sContexts;
    }

    @Override
    public List<String> getPTPCaption() {
        return Arrays.asList("'" + text + "'");
    }

    @Override
    public List<PrettyTreePrinter.Edge> getPTPEdges() {
        List<PrettyTreePrinter.Edge> res = new ArrayList<PrettyTreePrinter.Edge>();

        res.addAll(sContexts.stream().map(
                sc -> new PrettyTreePrinter.DefaultEdge("<s-context:" + sc.getRelation() + ">", sc, true)
        ).collect(Collectors.toList()));

        return res;
    }

    @Override
    public String toString() {
        return PrettyTreePrinter.prettyPrint(this, false, 40);
    }
}
