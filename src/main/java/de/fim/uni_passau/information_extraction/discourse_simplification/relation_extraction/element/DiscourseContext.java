package de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.element;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Matthias on 31.12.16.
 */
public class DiscourseContext implements PrettyTreePrinter.Node {
    private final String text;
    private final int sentenceIdx;
    private boolean sentSimContext;

    public DiscourseContext(String text, int sentenceIdx) {
        this.text = text;
        this.sentenceIdx = sentenceIdx;
        this.sentSimContext = false;
    }

    public void setSentSimContext() {
        this.sentSimContext = true;
    }

    public String getText() {
        return text;
    }

    public int getSentenceIdx() {
        return sentenceIdx;
    }

    public boolean isSentSimContext() {
        return sentSimContext;
    }

    @Override
    public List<String> getPTPCaption() {
        String sentSimContextStr = (sentSimContext)? " [s-context]" : "";
        return Arrays.asList("'" + text + "'" + sentSimContextStr);
    }

    @Override
    public List<PrettyTreePrinter.Edge> getPTPEdges() {
        return new ArrayList<PrettyTreePrinter.Edge>();
    }
}
