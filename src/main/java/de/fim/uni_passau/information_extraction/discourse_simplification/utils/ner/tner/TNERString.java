package de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.tner;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.NERString;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.NERTokenGroup;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeExtractionUtils;
import edu.stanford.nlp.trees.Tree;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 16.12.16.
 */
public class TNERString extends NERString {
    private final Tree parseTree;

    public TNERString(List<TNERToken> tokens, Tree parseTree) {
        super(tokens.stream().collect(Collectors.toList()));
        this.parseTree = parseTree;
        this.tokens.forEach(t -> ((TNERToken)t).setNerString(this));
    }

    public Tree getParseTree() {
        return parseTree;
    }
}
