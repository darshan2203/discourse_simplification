package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.DiscourseTree;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Subordination;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.words.WordsUtils;
import edu.stanford.nlp.ling.Word;

import java.util.List;

/**
 * Created by Matthias on 05.01.17.
 */
public class SubordinationExtraction extends Extraction {
    private final String extractionRule;
    private final Relation relation;
    private final String signalPhrase; // optional
    private final String leftConstituent;
    private final String rightConstituent;
    private final boolean superordinationIsLeft;
    private final Leaf.Type leftConstituentType;
    private final Leaf.Type rightConstituentType;

    // binary
    public SubordinationExtraction(String extractionRule, Relation relation, List<Word> signalPhraseWords, List<Word> leftConstituentWords, List<Word> rightConstituentWords, boolean superordinationIsLeft, Leaf.Type leftConstituentType, Leaf.Type rightConstituentType) {
        this.extractionRule = extractionRule;
        this.relation = relation;
        this.signalPhrase = (signalPhraseWords != null)? WordsUtils.wordsToString(signalPhraseWords) : null;
        this.leftConstituent = WordsUtils.wordsToProperSentenceString(leftConstituentWords);
        this.rightConstituent = WordsUtils.wordsToProperSentenceString(rightConstituentWords);
        this.superordinationIsLeft = superordinationIsLeft;
        this.leftConstituentType = leftConstituentType;
        this.rightConstituentType = rightConstituentType;
    }

    public DiscourseTree convert() {
        return new Subordination(
                extractionRule,
                relation,
                signalPhrase,
                new Leaf(leftConstituentType, extractionRule, leftConstituent),
                new Leaf(rightConstituentType, extractionRule, rightConstituent),
                superordinationIsLeft
        );
    }
}
