package de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.element;

import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.relation.DiscourseCoreContextRelation;
import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.relation.DiscourseCoreCoreRelation;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 08.12.16.
 */
public class DiscourseCore implements PrettyTreePrinter.Node {
    private final String text;
    private final int sentenceIdx;
    private List<DiscourseCoreCoreRelation> coreRelations;
    private List<DiscourseCoreContextRelation> contextRelations;

    public DiscourseCore(String text, int sentenceIdx) {
        this.text = text;
        this.sentenceIdx = sentenceIdx;
        this.coreRelations = new ArrayList<DiscourseCoreCoreRelation>();
        this.contextRelations = new ArrayList<DiscourseCoreContextRelation>();
    }

    public String getText() {
        return text;
    }

    public int getSentenceIdx() {
        return sentenceIdx;
    }

    public void addCoreRelation(DiscourseCoreCoreRelation coreRelation) {
        if (!coreRelations.contains(coreRelation)) {
            coreRelations.add(coreRelation);
        }
    }

    public List<DiscourseCoreCoreRelation> getCoreRelations() {
        return coreRelations;
    }

    public void addContextRelation(DiscourseCoreContextRelation contextRelation) {
        if (!contextRelations.contains(contextRelation)) {
            contextRelations.add(contextRelation);
        }
    }

    public List<DiscourseCoreContextRelation> getContextRelations() {
        return contextRelations;
    }

    @Override
    public List<String> getPTPCaption() {
        return Arrays.asList("'" + text + "'");
    }

    @Override
    public List<PrettyTreePrinter.Edge> getPTPEdges() {
        List<PrettyTreePrinter.Edge> res = new ArrayList<PrettyTreePrinter.Edge>();

        res.addAll(coreRelations.stream().map(
                cr -> new PrettyTreePrinter.DefaultEdge("<d-core:" + cr.getRelation() + ">", cr.getCore(), false)
        ).collect(Collectors.toList()));

        res.addAll(contextRelations.stream().map(
                cr -> new PrettyTreePrinter.DefaultEdge("<d-context:" + cr.getRelation() + ">", cr.getContext(), true)
        ).collect(Collectors.toList()));

        return res;
    }

    @Override
    public String toString() {
        return PrettyTreePrinter.prettyPrint(this, false, 40);
    }
}
