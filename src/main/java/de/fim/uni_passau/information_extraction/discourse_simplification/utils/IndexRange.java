package de.fim.uni_passau.information_extraction.discourse_simplification.utils;

/**
 * Created by Matthias on 13.12.16.
 */
public class IndexRange {
    private final int fromIdx;
    private final int toIdx;

    public IndexRange(int fromIdx, int toIdx) {
        this.fromIdx = fromIdx;
        this.toIdx = toIdx;
    }

    public int getFromIdx() {
        return fromIdx;
    }

    public int getToIdx() {
        return toIdx;
    }

    @Override
    public String toString() {
        return "(" + fromIdx + " | " + toIdx + ")";
    }
}
