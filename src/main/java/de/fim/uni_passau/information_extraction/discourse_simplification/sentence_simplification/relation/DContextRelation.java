package de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.relation;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element.DContext;

/**
 * Created by Matthias on 31.12.16.
 */
public class DContextRelation {
    private final Relation relation;
    private final DContext dContext;

    public DContextRelation(Relation relation, DContext dContext) {
        this.relation = relation;
        this.dContext = dContext;
    }

    public Relation getRelation() {
        return relation;
    }

    public DContext getDContext() {
        return dContext;
    }

    @Override
    public boolean equals(Object o) {
        return ((o instanceof DContextRelation)
                && (((DContextRelation) o).relation.equals(relation))
                && (((DContextRelation) o).dContext.equals(dContext)));
    }
}
