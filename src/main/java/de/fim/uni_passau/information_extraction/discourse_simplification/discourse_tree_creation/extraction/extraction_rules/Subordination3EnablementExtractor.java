package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction_rules;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree.Leaf;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.Extraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.ExtractionRule;
import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.extraction.extraction.SubordinationExtraction;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeExtractionUtils;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Matthias on 06.01.17.
 */
public class Subordination3EnablementExtractor extends ExtractionRule {

    @Override
    public Optional<Extraction> extract(Tree parseTree) {

        TregexPattern p = TregexPattern.compile("ROOT <<: (S < (NP $.. (VP=vp <+(VP) (NP|PP $.. (S=s)))))");
        TregexMatcher matcher = p.matcher(parseTree);

        if (matcher.findAt(parseTree)) {

            // the left, superordinate constituent
            List<Word> leftConstituentWords = new ArrayList<Word>();
            leftConstituentWords.addAll(ParseTreeExtractionUtils.getPrecedingWords(parseTree, matcher.getNode("s"), false));
            leftConstituentWords.addAll(ParseTreeExtractionUtils.getFollowingWords(parseTree, matcher.getNode("s"), false));

            // the right, subordinate constituent
            List<Word> rightConstituentWords = ParseTreeExtractionUtils.getContainingWords(matcher.getNode("s"));

            // result
            Optional<Relation> relation = Optional.empty();
            Leaf.Type leftConstituentType = Leaf.Type.DEFAULT;
            Leaf.Type rightConstituentType = Leaf.Type.DEFAULT;

            // enablement
            if (!relation.isPresent()) {
                if (isInfinitival(matcher.getNode("s"))) {
                    relation = Optional.of(Relation.ENABLEMENT);
                    rightConstituentWords = rephraseEnablement(matcher.getNode("s"), matcher.getNode("vp"));
                    rightConstituentType = Leaf.Type.SENT_SIM_CONTEXT;
                }
            }

            if (relation.isPresent()) {
                Extraction res = new SubordinationExtraction(
                        getClass().getSimpleName(),
                        relation.get(),
                        null,
                        leftConstituentWords, // the superordinate constituent
                        rightConstituentWords, // the subordinate constituent
                        true,
                        leftConstituentType,
                        rightConstituentType
                );

                return Optional.of(res);
            }
        }

        return Optional.empty();
    }
}
