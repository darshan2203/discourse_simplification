package de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.discourse_tree;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.PrettyTreePrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 30.12.16.
 */
public class Coordination extends DiscourseTree {
    protected final Relation relation;
    private final String signalPhrase; // optional
    private List<DiscourseTree> coordinations;

    public Coordination(String extractionRule, Relation relation, String signalPhrase, List<DiscourseTree> coordinations) {
        super(extractionRule);
        this.relation = relation;
        this.signalPhrase = signalPhrase;
        this.coordinations = new ArrayList<DiscourseTree>();
        coordinations.forEach(c -> addCoordination(c));
    }

    public void addCoordination(DiscourseTree coordination) {
        this.coordinations.add(coordination);
        coordination.parent = this;
    }

    public void invalidateCoordination(DiscourseTree coordination) {
        replaceCoordination(coordination, new Invalidation());
    }

    public void replaceCoordination(DiscourseTree oldCoordination, DiscourseTree newCoordination) {
        for (int i = 0; i < coordinations.size(); i++) {
            if (coordinations.get(i).equals(oldCoordination)) {
                coordinations.set(i, newCoordination);
                newCoordination.parent = this;
                newCoordination.setRecursiveUnsetSentenceIdx(oldCoordination.getSentenceIdx());
                break;
            }
        }
    }

    public void removeInvalidations() {
        for (int i = coordinations.size() - 1; i >= 0 ; i--) {
            if (coordinations.get(i) instanceof Invalidation) {
                coordinations.remove(i);
            }
        }
    }

    public Relation getRelation() {
        return relation;
    }

    public List<DiscourseTree> getCoordinations() {
        return coordinations;
    }

    public List<DiscourseTree> getOtherCoordinations(DiscourseTree coordination) {
        return coordinations.stream().filter(c -> !c.equals(coordination)).collect(Collectors.toList());
    }

    public List<DiscourseTree> getOtherPrecedingCoordinations(DiscourseTree coordination) {
        List<DiscourseTree> res = new ArrayList<DiscourseTree>();

        for (DiscourseTree child : coordinations) {
            if (child.equals(coordination)) {
                break;
            } else {
                res.add(child);
            }
        }

        return res;
    }

    public List<DiscourseTree> getOtherFollowingCoordinations(DiscourseTree coordination) {
        List<DiscourseTree> res = new ArrayList<DiscourseTree>();

        boolean found = false;
        for (DiscourseTree child : coordinations) {
            if (child.equals(coordination)) {
                found = true;
            } else {
                if (found) {
                    res.add(child);
                }
            }
        }

        return res;
    }

    @Override
    public List<String> getPTPCaption() {
        String signalPhraseStr = (signalPhrase != null)? "'" + signalPhrase + "'" : "NULL";
        return Arrays.asList("CO/" + relation + " (" + signalPhraseStr + ", " + extractionRule + ")");
    }

    @Override
    public List<PrettyTreePrinter.Edge> getPTPEdges() {
        return coordinations.stream().map(c -> new PrettyTreePrinter.DefaultEdge("n", c, true)).collect(Collectors.toList());
    }

}
