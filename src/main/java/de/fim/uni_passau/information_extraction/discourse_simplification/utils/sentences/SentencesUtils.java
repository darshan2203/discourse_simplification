package de.fim.uni_passau.information_extraction.discourse_simplification.utils.sentences;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.process.DocumentPreprocessor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthias on 13.12.16.
 */
public class SentencesUtils {

    private static List<String> splitIntoSentences(Reader reader) {
        List<String> res = new ArrayList<String>();

        DocumentPreprocessor dp = new DocumentPreprocessor(reader);
        for (List<HasWord> sentence : dp) {
            res.add(Sentence.listToString(sentence));
        }

        return res;
    }

    public static List<String> splitIntoSentences(String text) {
        return splitIntoSentences(new StringReader(text));
    }

    public static List<String> splitIntoSentencesFromFile(File file) throws FileNotFoundException {
        return splitIntoSentences(new BufferedReader(new FileReader(file)));
    }
}
