package de.fim.uni_passau.information_extraction.discourse_simplification.processing;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.DiscourseTreeCreator;
import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.DiscourseExtractor;
import de.fim.uni_passau.information_extraction.discourse_simplification.relation_extraction.element.DiscourseCore;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.Simplifier;
import de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.element.DCore;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.sentences.SentencesUtils;
import de.fim.uni_passau.information_extraction.wikipedia.Wikipedia;
import de.fim.uni_passau.information_extraction.wikipedia.WikipediaAccessException;
import de.fim.uni_passau.information_extraction.wikipedia.WikipediaArticle;
import de.fim.uni_passau.information_extraction.wikipedia.WikipediaArticleNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Matthias on 09.12.16.
 */
public class Processor {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public static enum ProcessingType {
        SEPARATE,
        WHOLE
    }

    public static interface ProcessMethod {
        List<DCore> process(List<String> sentences);
    }

    private static final Wikipedia WIKIPEDIA = new Wikipedia("en.wikipedia.org");
    private static final File WIKIPEDIA_TITLES_FILE = new File("wikipedia_titles.txt");
    private final static DiscourseTreeCreator DISCOURSE_TREE_CREATOR = new DiscourseTreeCreator();
    private final static DiscourseExtractor DISCOURSE_EXTRACTOR = new DiscourseExtractor();
    private final static Simplifier SIMPLIFIER = new Simplifier();

    private static List<String> readWikipediaTitles() throws IOException {
        List<String> res = new ArrayList<String>();

        BufferedReader br = new BufferedReader(new FileReader(WIKIPEDIA_TITLES_FILE));
        String line;
        while ((line = br.readLine()) != null) {
            if (line.trim().length() > 0) {
                res.add(line.trim());
            }
        }

        return res;
    }

    protected List<DCore> processWikipedia(int nrArticles, Integer minArticleSentences, ProcessMethod processMethod) {
        final Random rand = new Random();

        List<DCore> res = new ArrayList<>();

        // try to get wikipedia titles
        List<String> wikipediaTitles;
        try {
            wikipediaTitles = readWikipediaTitles();
        } catch (IOException e) {
            logger.error("Failed to evaluate wikipedia articles.\nCould not load wikipedia titles from file: '" + WIKIPEDIA_TITLES_FILE.getAbsolutePath() + "'");
            return res;
        }

        int processed = 0;
        while ((wikipediaTitles.size() > 0) && (processed < nrArticles)) {
            int idx = rand.nextInt(wikipediaTitles.size());
            String article = wikipediaTitles.remove(idx);

            try {
                WikipediaArticle a = WIKIPEDIA.fetchArticle(article, false);
                List<String> sentences = SentencesUtils.splitIntoSentences(a.getCleanedText());

                // check if article should be skipped
                if ((minArticleSentences != null) && (sentences.size() < minArticleSentences)) {
                    // skip article
                } else {
                    logger.info("Processing wikipedia article '" + article + "'");

                    // process article
                    res.addAll(processMethod.process(sentences));

                    ++processed;
                }
            } catch (WikipediaArticleNotFoundException e) {
                // skip article
            } catch (WikipediaAccessException e) {
                // skip article
            }
        }
        logger.info("Processed " + processed + " wikipedia articles.");

        return res;
    }

    public List<DCore> processWikipedia(int nrArticles, Integer minArticleSentences, ProcessingType type) {

        return processWikipedia(nrArticles, minArticleSentences, new ProcessMethod() {
            @Override
            public List<DCore> process(List<String> sentences) {
                return Processor.this.process(sentences, type);
            }
        });
    }

    protected List<DCore> processWikipediaArticles(List<String> articles, ProcessMethod processMethod) {
        List<DCore> res = new ArrayList<>();

        for (String article : articles) {
            try {
                WikipediaArticle a = WIKIPEDIA.fetchArticle(article, false);
                List<String> sentences = SentencesUtils.splitIntoSentences(a.getCleanedText());

                logger.info("Processing wikipedia article '" + article + "'");

                // process article
                res.addAll(processMethod.process(sentences));

            } catch (WikipediaArticleNotFoundException e) {
                // skip article
            } catch (WikipediaAccessException e) {
                // skip article
            }
        }

        return res;
    }

    public List<DCore> processWikipediaArticles(List<String> articles, ProcessingType type) {
        return processWikipediaArticles(articles, new ProcessMethod() {
            @Override
            public List<DCore> process(List<String> sentences) {
                return Processor.this.process(sentences, type);
            }
        });
    }

    public List<DCore> process(File file, ProcessingType type) throws FileNotFoundException {
        List<String> sentences = SentencesUtils.splitIntoSentencesFromFile(file);
        return process(sentences, type);
    }

    public List<DCore> process(String text, ProcessingType type) {
        List<String> sentences = SentencesUtils.splitIntoSentences(text);
        return process(sentences, type);
    }

    public List<DCore> process(List<String> sentences, ProcessingType type) {
        if (type.equals(ProcessingType.SEPARATE)) {
            return processSeparate(sentences);
        } else if (type.equals(ProcessingType.WHOLE)) {
            return processWhole(sentences);
        } else {
            throw new IllegalArgumentException("Unknown ProcessingType.");
        }
    }

    // creates one discourse tree over all sentences (investigates intra-sentential and inter-sentential relations)
    private List<DCore> processWhole(List<String> sentences) {
        List<DCore> res = new ArrayList<DCore>();

        // Step 1) create document discourse tree
        logger.info("Step 1) Create document discourse tree");
        DISCOURSE_TREE_CREATOR.reset();

        int idx = 0;
        for (String sentence : sentences) {
            logger.info("### Processing sentence ###");
            logger.info(sentence.toString());

            // extend discourse tree
            DISCOURSE_TREE_CREATOR.addSentence(sentence, idx);
            DISCOURSE_TREE_CREATOR.update();
            if (logger.isDebugEnabled()) {
                logger.debug(DISCOURSE_TREE_CREATOR.getLastSentenceTree().get().toString()); // to show a subtree of the document discourse tree only
//                logger.debug(DISCOURSE_TREE_CREATOR.getDiscourseTree().toString()); // to show the current document discourse tree
            }

            ++idx;
        }

        // Step 2) extract discourse cores
        logger.info("Step 2) extract discourse cores");

        List<DiscourseCore> discourseCores = DISCOURSE_EXTRACTOR.extract(DISCOURSE_TREE_CREATOR.getDiscourseTree());
        if (logger.isDebugEnabled()) {
            discourseCores.forEach(x -> logger.debug(x.toString()));
        }

        // Step 3) generate output format
        logger.info("Step 3) Generate output format");

        List<DCore> dCores = SIMPLIFIER.simplify(discourseCores);
        res.addAll(dCores);

        if (logger.isInfoEnabled()) {
            dCores.forEach(core -> logger.info(core.toString()));
        }

        return res;
    }

    // creates discourse trees for each individual sentence (investigates intra-sentential relations only)
    private List<DCore> processSeparate(List<String> sentences) {
        List<DCore> res = new ArrayList<DCore>();

        int idx = 0;
        for (String sentence : sentences) {
            logger.info("### Processing sentence ###");
            logger.info("'" + sentence + "'");

            // Step 1) create sentence discourse tree
            logger.debug("Step 1) Create sentence discourse tree");
            DISCOURSE_TREE_CREATOR.reset();
            DISCOURSE_TREE_CREATOR.addSentence(sentence, idx);
            DISCOURSE_TREE_CREATOR.update();
            if (logger.isDebugEnabled()) {
                logger.debug(DISCOURSE_TREE_CREATOR.getDiscourseTree().toString());
            }

            // Step 2) extract discourse cores
            logger.debug("Step 2) extract discourse cores");

            List<DiscourseCore> discourseCores = DISCOURSE_EXTRACTOR.extract(DISCOURSE_TREE_CREATOR.getDiscourseTree());
            if (logger.isDebugEnabled()) {
                discourseCores.forEach(x -> logger.debug(x.toString()));
            }

            // Step 3) generate output format
            logger.debug("Step 3) generate output format");

            List<DCore> dCores = SIMPLIFIER.simplify(discourseCores);
            res.addAll(dCores);

            if (logger.isInfoEnabled()) {
                dCores.forEach(core -> logger.info(core.toString()));
            }

            ++idx;
        }

        return res;
    }
}
