package de.fim.uni_passau.information_extraction.discourse_simplification.sentence_simplification.classification;

import de.fim.uni_passau.information_extraction.discourse_simplification.discourse_tree_creation.Relation;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.NERStringParseException;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.NERStringParser;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.ner.tner.TNERString;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeException;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeParser;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.parseTree.ParseTreeVisualizer;
import de.fim.uni_passau.information_extraction.discourse_simplification.utils.words.WordsUtils;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.tregex.TregexMatcher;
import edu.stanford.nlp.trees.tregex.TregexPattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Matthias on 29.01.17.
 */
public class SContextClassifier {
    private static final Logger LOGGER = LoggerFactory.getLogger(SContextClassifier.class);

    private static final String PATTERN_PREFIX = "^.*(?<!\\w)";
    private static final String PATTERN_SUFFIX = "(?!\\w).*$";

    private static boolean isTimeNP(Tree np) {
        final List<String> monthPatterns = Arrays.asList(
               "january", "jan.",
                "february", "feb.",
                "march", "mar.",
                "april", "apr.",
                "may",
                "june",
                "july",
                "august", "aug.",
                "september", "sept.",
                "october", "oct.",
                "november", "nov.",
                "december", "dec."
        ).stream().map(p -> PATTERN_PREFIX + p + PATTERN_SUFFIX).collect(Collectors.toList());

        final List<String> days = Arrays.asList(
                "monday", "mon.",
                "tuesday", "tues.",
                "wednesday", "wed.",
                "thursday", "thurs.",
                "friday", "fri.",
                "saturday", "sat.",
                "sunday", "sun."
        ).stream().map(p -> PATTERN_PREFIX + p + PATTERN_SUFFIX).collect(Collectors.toList());

        final String yearPattern = PATTERN_PREFIX + "[1-2]\\d\\d\\d" + PATTERN_SUFFIX;
        final String bcadPattern = PATTERN_PREFIX + "(\\d+\\s+(bc|ad)|ad\\s+\\d+)" + PATTERN_SUFFIX;
        final String centuryPattern = PATTERN_PREFIX + "(1st|2nd|3rd|\\d+th)\\s+century" + PATTERN_SUFFIX;
        final String timePattern = PATTERN_PREFIX + "([0-1]?\\d|2[0-4])\\s*:\\s*[0-5]\\d" + PATTERN_SUFFIX;

        String text = WordsUtils.wordsToString(np.yieldWords()).toLowerCase();
        return ((monthPatterns.stream().anyMatch(p -> text.matches(p)))
                || (days.stream().anyMatch(p -> text.matches(p)))
                || (text.matches(yearPattern))
                || (text.matches(bcadPattern))
                || (text.matches(centuryPattern))
                || (text.matches(timePattern)));
    }

    private static boolean isLocationNP(Tree np) {
        try {
            TNERString ner = NERStringParser.parse(np);

            return ner.getTokens().stream().anyMatch(t -> t.getCategory().equals("LOCATION"));
        } catch (NERStringParseException e) {
            return false;
        }
    }

    public static Optional<Relation> classify(String sContext) {

        try {
            Tree parseTree = ParseTreeParser.parse(sContext);

            // find TIME-relation
            TregexPattern p = TregexPattern.compile("ROOT <<, (/This/ . (/(is|was)/ . (/(in|at|around)/ . NP=np)))");
            TregexMatcher matcher = p.matcher(parseTree);

            if (matcher.findAt(parseTree)) {
                if (isTimeNP(matcher.getNode("np"))) {
                    return Optional.of(Relation.TIME);
                }
            }

            // find LOCATION-relation
            p = TregexPattern.compile("ROOT <<, (/This/ . (/(is|was)/ . (__ . NP=np)))");
            matcher = p.matcher(parseTree);

            if (matcher.findAt(parseTree)) {
                if (isLocationNP(matcher.getNode("np"))) {
                    return Optional.of(Relation.LOCATION);
                }
            }


        } catch (ParseTreeException e) {
            LOGGER.error("Could not generate parse tree for sContext: '" + sContext + "'");
        }

        return Optional.empty();
    }
}
