package de.fim.uni_passau.information_extraction.discourse_simplification.utils;

import de.fim.uni_passau.information_extraction.discourse_simplification.utils.words.WordsUtils;
import edu.stanford.nlp.ling.Word;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Matthias on 29.11.16.
 */
public class ExtractionUtilsTest {

    @Test
    public void wordsToProperSentence() throws Exception {
        List<Word> words = Arrays.asList(
          new Word("."),
          new Word("."),
          new Word("hello"),
          new Word(","),
          new Word(","),
          new Word("this"),
          new Word("is"),
          new Word("a"),
          new Word("test"),
          new Word("."),
          new Word(".")
        );

        String sentence = WordsUtils.wordsToProperSentenceString(words);
        assertEquals("Hello , this is a test .", sentence);
    }
}